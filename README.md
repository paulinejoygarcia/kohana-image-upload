# Kohana Image Upload Test App

Image file upload with thumbnails and records CRUD using Kohana 3 and jQuery AJAX.

## Installation
1. Download source code or clone the repository
2. Extract the files into a directory where the web server can access it
3. Assign read and write permissions to the /_uploads/ and /_uploads/_thumbnails/ directories
4. Run the SQL script from /_database/ directory
5. Update base_url in /application/bootstrap.php
6. Update database configuration in /modules/database/config/database.php
7. Test the app by accessing base_url in your browser