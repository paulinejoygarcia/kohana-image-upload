$(document).ready(function () {
    fetch();

    $(document).on('click', '#add-record', function (e) {
        $('#upload-form-title').text('Add Record');
        $('#record-id').val('');
        $('#title').val('');
        $('#file-info').hide();
        $('.file-box label').text('Choose an image');
        $('#file-thumbnail').hide();
    });

    $(document).on('click', '#edit-record', function (e) {
        $('#upload-form-title').text('Edit Record');
        $('#record-id').val($(this).data('id'));
        $('#file-info').hide();
        $('.file-box label').text('Choose an image');
        fetch($(this).data('id'));
    });

    $(document).on('click', '#delete-record', function (e) {
        if (confirm('Are you sure you want to delete this record?')) {
            $.post(`${baseUrl}ajax/delete/${$(this).data('id')}`, function (result) {
                result = JSON.parse(result) || {};
                fetch();
            });
        }
    });

    $(document).on('click', '.full-image', function (e) {
        $('div.image-viewer').html(`<img src="${$(this).data('image')}"/>`);
        $('div#view-image div').width($('div.image-viewer img').width());
        $('div#view-image div').height($('div.image-viewer img').height())
    });

    $(document).on('change', '[type=file]', function () {
        const file = this.files[0];
        let formData = new FormData();
        formData.append('file', file);
        $('#file-info').slideDown();
        $('#file-thumbnail').hide();
        if (file.name.length >= 30) {
            $('#file-info .file-name span').empty().append(file.name.substr(0, 30) + '...');
        } else {
            $('#file-info .file-name span').empty().append(file.name);
        }
        if (file.size >= 1073741824) {
            $('#file-info .file-size span').empty().append(Math.round(file.size / 1073741824) + 'GB');
        } else if (file.size >= 1048576) {
            $('#file-info .file-size span').empty().append(Math.round(file.size / 1048576) + 'MB');
        } else if (file.size >= 1024) {
            $('#file-info .file-size span').empty().append(Math.round(file.size / 1024) + 'KB');
        } else {
            $('#file-info .file-size span').empty().append(Math.round(file.size) + 'B');
        }
        $('#file-info .file-type span').empty().append(file.type);
        if (file.name.length >= 30) {
            $('.file-box label').text('Chosen: ' + file.name.substr(0, 30) + '...');
        } else {
            $('.file-box label').text('Chosen: ' + file.name);
        }

        let ext = $('#file').val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['jpg', 'jpeg', 'png', 'gif']) === -1) {
            $('#file-info').hide();
            $('.file-box label').text('Choose an image');
            $('#file').val('');
            alert('Invalid file type!');
        }

    });

    $(document).on('click', '#save', function (e) {
        const data = new FormData($('#upload-form')[0]);
        $.ajax({
            url: `${baseUrl}ajax/save/${$('#record-id').val() || ''}`,
            type: 'POST',
            success: function (result) {
                result = JSON.parse(result) || {};
                if (result && result.data) {
                    alert(result.data);
                }
                fetch();
                window.location.hash = '#close';
            },
            timeout: 5000,
            processData: false,
            contentType: false,
            cache: false,
            data
        });
        return false;
    });
});

function fetch(id) {
    $.get(`${baseUrl}ajax/fetch/${id || ''}`, function (result) {
        result = JSON.parse(result) || [];
        if (id) {
            if (result && result.success) {
                $('form #title').val(result.data.title);
                $('#file-thumbnail').html(`<img src="${result.data.thumbnail}"/>`);
                $('#file-thumbnail').show();
            }
        } else {
            let table = $('table#records tbody');
            table.html('');

            if (result && ((!result.success) || (result.success && !result.data.length))) {
                insertTableRow(table);
            } else {
                result.data.forEach(record => {
                    insertTableRow(table, record);
                });
            }
        }
    });
}

function insertTableRow(table, record) {
    if (!table)
        return;

    if (!record) {
        table.append(
            `<tr>
                <td colspan="5">No records found</td>
            </tr>`
        );
    } else {
        table.append(
            `<tr>
                <td data-label="Title">
                    <a href="#view-image" class="full-image" data-image=${record.image}>
                        ${record.title}
                    </a>
                </td>
                <td data-label="Thumbnail">
                    <a href="#view-image" class="full-image" data-image=${record.image}>
                        <img src="${record.thumbnail}" alt="${record.filename}"/>
                    </a>
                </td>
                <td data-label="Filename">${record.filename}</td>
                <td data-label="Upload Date">${record.created_at}</td>
                <td data-label="Actions">
                    <a id="edit-record" data-id="${record.id}" href="#open-form" class="button button-warning">Edit</a>
                    <button id="delete-record" data-id="${record.id}" class="button button-danger">Delete</button>
                </td>
            </tr>`
        );
    }
}