<a id="add-record" href="#open-form" class="button button-success">Add</a>

<table id="records" class="table">
    <thead>
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Thumbnail</th>
            <th scope="col">Filename</th>
            <th scope="col">Date Added</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody></tbody>
</table>

<div id="open-form" class="modal-dialog">
    <div>
        <a href="#close" title="Close" class="modal-close">&#10006;</a>
        <div class="form">
            <h1 id="upload-form-title">Upload Image</h1>
            <form id="upload-form">
                <input id="record-id" type="hidden" name="record_id" />
                <input id="title" type="text" name="title" placeholder="Title" />
                <div class="file-box">
                    <label for="file" class="file-label button button-warning">Choose an image</label>
                    <input id="file" type="file" name="image" />
                </div>
                <div id="file-info">
                    <p class="file-name">File Name: <span></span></p>
                    <p class="file-size">File Size: <span></span></p>
                    <p class="file-type">File Type: <span></span></p>
                </div>
                <div id="file-thumbnail"></div>
                <button id="save" class="button button-primary">Save</button>
                <a href="#close" class="button">Cancel</a>
            </form>
        </div>
    </div>
</div>

<div id="view-image" class="modal-dialog">
    <div>
        <a href="#close" title="Close" class="modal-close">&#10006;</a>
        <div class="image-viewer"></div>
    </div>
</div>