<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Image Upload App</title>
        <?php echo html::style('_assets/css/style.css'); ?>
    </head>
    <body>
    <div class="custom">
        <h1>Image Upload App</h1>
        <hr>
        <?php echo isset($content) ? $content : ''?>
    </div>
    <?php echo html::script('_assets/js/jquery-3.2.1.min.js'); ?>
    <script type="text/javascript">
        let baseUrl = '<?php echo url::base()?>';
    </script>
    <?php
        if(isset($scripts)) {
            foreach($scripts as $script) {
                echo html::script('_scripts/' . $script . '.js');
            }
        }
    ?>
    </body>
</html>
