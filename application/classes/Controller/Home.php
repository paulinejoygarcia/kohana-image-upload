<?php defined('SYSPATH') OR die('No Direct Script Access');

Class Controller_Home extends Controller_Template
{
    public $template = 'layout';
    
	public function action_index()
	{
        $this->template->content = View::factory('home');
        $this->template->scripts = array('home');
	}
}