<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {
    function before() {
        if(!$this->request->is_ajax()) {
            die('Expecting an Ajax request');
        }
    }

    private $content;

    function content($content = null) {
        if(is_null( $content )) {
            return $this->content;
        }
        $this->content = $content;
    }

    function after() {
        $this->response->body(json_encode( $this->content ));
    }

    public function action_fetch() {
        if($this->request->method() == Request::GET) {
            $record_id = $this->request->param('id');
            $result = array();
            try {
                if($record_id) {
                    $record = ORM::factory('Image', $record_id);
                    if($record) {
                        $result['title'] = $record->title;
                        $result['thumbnail'] = URL::base().'_uploads/_thumbnails/'.$record->filename;
                    } else {
                        $this->content = array(
                            'success' => false,
                            'data' => 'Record not found',
                        );
                        return;
                    }
                } else {
                    $records = ORM::factory('Image')->order_by('created_at', 'desc')->find_all();
                    foreach($records as $rec) {
                        $result[] = array(
                            'id' => $rec->id,
                            'title' => $rec->title,
                            'filename' => $rec->filename,
                            'image' => URL::base().'_uploads/'.$rec->filename,
                            'thumbnail' => URL::base().'_uploads/_thumbnails/'.$rec->filename,
                            'created_at' => date('M j, Y h:i A', strtotime($rec->created_at))
                        );
                    }
                }
            } catch (ORM_Validation_Exception $e) {
                $this->content = array(
                    'success' => false,
                    'data' => 'Error while retrieving record(s). Please try again later.',
                );
                return;
            }
            $this->content = array(
                'success' => true,
                'data' => $result
            );
        } else {
            $this->content = array(
                'success' => false,
                'data' => 'Method not allowed. Only `GET` requests are supported'
            );
        }
    }

    public function action_save() {
        if($this->request->method() == Request::POST) {
            $uploaded = array('success' => true);
            $title = $this->request->post('title');
            $record_id = $this->request->param('id');

            if(!$title) {
                $this->content = array(
                    'success' => false,
                    'data' => 'The title is required'
                );
                return;
            }
            if(!$record_id && (!isset($_FILES['image']) || $_FILES['image']['size'] == 0)) {
                $this->content = array(
                    'success' => false,
                    'data' => 'The image is required'
                );
                return;
            }

            if(isset($_FILES['image']) && $_FILES['image']['size'] > 0)
                $uploaded = $this->_process_upload($_FILES['image']);

            if(!$uploaded['success']) {
                $this->content = $uploaded;
                return;
            }

            try {
                $record = ORM::factory('Image', $record_id ? $record_id : NULL);
                $record->title =  $title;
                if(isset($uploaded['data'])) {
                    $record->filename = $uploaded['data'];
                }
                $record->save();

                $this->content = array(
                    'success' => true,
                    'data' => 'Record successfully ' . ($record_id ? 'updated' : 'inserted'),
                );
            } catch (ORM_Validation_Exception $e) {
                $this->content = array(
                    'success' => false,
                    'data' => 'Error while saving record. Please try again later.',
                );
                return;
            }
        } else {
            $this->content = array(
                'success' => false,
                'data' => 'Method not allowed. Only `POST` requests are supported'
            );
        }
    }

    public function action_delete() {
        if($this->request->method() == Request::POST) {
            $record_id = $this->request->param('id');
            $record = ORM::factory('Image', $record_id);
            if(!$record) {
                $this->content = array(
                    'success' => false,
                    'data' => 'Record not found',
                );
                return;
            }

            if(file_exists(DOCROOT.'/_uploads/'.$record->filename))
                unlink(DOCROOT.'/_uploads/'.$record->filename);

            if(file_exists(DOCROOT.'/_uploads/_thumbnails/'.$record->filename))
                unlink(DOCROOT.'/_uploads/_thumbnails/'.$record->filename);

            $record->delete();

            $this->content = array(
                'success' => true,
                'data' => 'Record successfully deleted'
            );
        } else {
            $this->content = array(
                'success' => false,
                'data' => 'Method not allowed. Only `POST` requests are supported'
            );
        }
    }
    
    protected function _process_upload($image) {
        if(!Upload::valid($image) OR !Upload::not_empty($image)) {
            return array(
                'success' => false, 
                'data' => 'Invalid file'
            );
        }

        if(!Upload::type($image, array('jpg', 'jpeg', 'png', 'gif'))) {
            return array(
                'success' => false, 
                'data' => 'Invalid file type. Supported file types: jpg, jpeg,  png, gif'
            );
        }

        if(!Upload::size($image, '5M')) {
            return array(
                'success' => false, 
                'data' => 'Please upload an image lesser than 5MB'
            );
        }

        $directory = DOCROOT . '_uploads/';
        $file = Upload::save($image, NULL, $directory);
        if ($file) {
            $explode = explode('.', $image['name']);
            $ext = array_pop($explode);
            $filename = strtolower(Text::random('alnum', 20)) . '.' . $ext;

            $img = Image::factory($file);

            //Save original size
            $img->save($directory . $filename);
 
            //Save thumbnail
            $img->resize(200, 200, Image::AUTO)
                ->save($directory . '/_thumbnails/' . $filename);
 
            unlink($file);

            return array(
                'success' => true, 
                'data' => $filename
            );
        }
 
        return array(
            'success' => false, 
            'data' => 'There was a problem uploading the file, please try again later.'
        );
    }
}
